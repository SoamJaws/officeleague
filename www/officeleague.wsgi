#!/usr/bin/env python
import sys
import os

sys.path.insert(0,"/var/www/officeleague")

from officeleague import app as application

class ReverseProxied(object):
    def __init__(self, app, script_name=None):
        self.app = app
        self.script_name = script_name

    def __call__(self, environ, start_response):
        script_name = environ.get('HTTP_X_SCRIPT_NAME', '') or self.script_name
        if script_name:
            environ['SCRIPT_NAME'] = script_name
            path_info = environ['PATH_INFO']
            if path_info.startswith(script_name):
                environ['PATH_INFO'] = path_info[len(script_name):]

        scheme = environ.get('HTTP_X_SCHEME', '')
        if scheme:
            environ['wsgi.url_scheme'] = scheme
        return self.app(environ, start_response)

if __name__ == '__main__':
    application.wsgi_app = ReverseProxied(application.wsgi_app, script_name=os.environ.get('APPLICATION_ROOT'))
    application.run(debug=True,host='0.0.0.0')
