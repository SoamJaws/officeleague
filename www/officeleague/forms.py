from officeleague import app

from flask_wtf import FlaskForm
from flask_wtf.csrf import CSRFProtect
from wtforms import StringField, IntegerField, BooleanField, SubmitField

from wtforms import validators, ValidationError

CSRFProtect(app)

class GameForm(FlaskForm):
    home = StringField("Home player", [validators.InputRequired("Please enter the name of the home player."), validators.Regexp('^[a-zA-Z0-9]*$', message="Name may only contain letters and numbers")])
    away = StringField("Away player", [validators.InputRequired("Please enter the name of the away player."), validators.Regexp('^[a-zA-Z0-9]*$', message="Name may only contain letters and numbers")])
    home_score = IntegerField("Home score",[validators.InputRequired("Please enter the home score.")])
    away_score = IntegerField("Away score",[validators.InputRequired("Please enter the away score.")])
    game_edition = StringField("Game name",[validators.InputRequired("Please enter the played game")])
    overtime = BooleanField('Overtime', default=False)
    submit = SubmitField("Add game")

    def validate(self):
        result = True
        if not FlaskForm.validate(self):
            result = False
        if self.home.data == self.away.data:
            self.home.errors.append("Please enter distinct home and away player names")
            result = False
        if self.home_score.data < 0:
            self.home_score.errors.append("Please enter a non negative score")
            result = False
        if self.away_score.data < 0:
            self.away_score.errors.append("Please enter a non negative score")
            result = False
        if self.home_score.data == self.away_score.data:
            self.home_score.errors.append("Please enter a non tied score")
            result = False
        if self.overtime.data and abs(self.home_score.data - self.away_score.data) > 1:
            self.home_score.errors.append("Overtime results cannot differ with more than 1")
            result = False

        return result
