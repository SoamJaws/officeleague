from officeleague import app

import model
import forms

from flask import render_template, redirect, request, session, url_for

app.config['SECRET_KEY'] = 'secret'

@app.route("/", methods=['POST', 'GET'])
def stats():
    form = forms.GameForm()
    if request.method == 'POST' and form.validate():
        home = request.form["home"]
        away = request.form["away"]
        home_score = request.form["home_score"]
        away_score = request.form["away_score"]
        game = request.form["game_edition"]
        model.add_game(home, away, home_score, away_score, game, "overtime" in request.form)
        return redirect(url_for('stats'))
    else:
        return render_template('stats.html', form=form, players=model.get_players(), game_editions=model.get_played_game_editions(), current_game_edition=model.get_current_game_edition())

@app.route("/league_title")
def league_title():
    return model.get_league_title()

@app.route("/players")
def players():
    return model.get_players()

@app.route("/ppg_stats")
def ppg_stats():
    (year, month) = model.current_season()
    return model.get_ppg_stats(year, month)

@app.route("/overall_ppg")
def overall_ppg():
    return model.get_overall_ppg_stats()

@app.route("/game_results")
def game_results():
    (year, month) = model.current_season()
    return model.get_game_results(year, month)

@app.route("/recent_games")
def recent_games():
    return model.get_recent_games()

@app.route("/ppg_champions")
def ppg_champions():
    return model.get_ppg_champions(model.exclude_current_season())

@app.route("/ppg_record")
def ppg_record():
    return model.get_ppg_record()

@app.route("/current_game_edition")
def current_game_edition():
    return model.get_current_game_edition()
