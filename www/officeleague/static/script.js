function loadLeagueTitle() {
  $.ajax({
    url: $SCRIPT_ROOT + "/league_title",
    success: function(data) {
      $("#league_title").html("")
      $("#league_title").text("OFFICELEAGUE " + data)
    },
    complete: function() {
      setTimeout(loadLeagueTitle, 3000)
    }
  });
}

function loadPpgStats() {
  $.ajax({
    url: $SCRIPT_ROOT + "/ppg_stats",
    success:  function(data) {
      var header = $("<tr></tr>").addClass("header")
      header.append($("<th></th>").text("#"))
      header.append($("<th></th>").text("Player"))
      header.append($("<th></th>").attr("title", "Win loss ratio").text("PPG"))
      header.append($("<th></th>").attr("title", "Games played").text("GP"))
      header.append($("<th></th>").attr("title", "Wins").text("W"))
      header.append($("<th></th>").attr("title", "Losses").text("L"))
      header.append($("<th></th>").attr("title", "Overtime wins").text("OTW"))
      header.append($("<th></th>").attr("title", "Overtime losses").text("OTL"))
      header.append($("<th></th>").attr("title", "Goals for").text("GF"))
      header.append($("<th></th>").attr("title", "Goals against").text("GA"))
      header.append($("<th></th>").attr("title", "Goal difference").text("DIFF"))
      $("#ppg_stats").html("")
      $("#ppg_stats").append($("<caption></caption>").text("PPG " + data["season"]))
      $("#ppg_stats").append(header)
      i = 0
      data["stats"].forEach(function(stats, i) {
        var row = $("<tr></tr>").addClass("player")
        row.append($("<th></th>").text(i+1))
        row.append($("<th></th>").text(stats["Player"]))
        row.append($("<th></th>").text(stats["PPG"]))
        row.append($("<th></th>").text(stats["GP"]))
        row.append($("<th></th>").text(stats["W"]))
        row.append($("<th></th>").text(stats["L"]))
        row.append($("<th></th>").text(stats["OTW"]))
        row.append($("<th></th>").text(stats["OTL"]))
        row.append($("<th></th>").text(stats["GF"]))
        row.append($("<th></th>").text(stats["GA"]))
        row.append($("<th></th>").text(stats["DIFF"]))
        $("#ppg_stats").append(row)
      })
    },
    complete: function() {
      setTimeout(loadPpgStats, 3000)
    }
  });
}

function loadOverallPpg() {
  $.ajax({
    url: $SCRIPT_ROOT + "/overall_ppg",
    success:  function(data) {
      var header = $("<tr></tr>").addClass("header")
      header.append($("<th></th>").text("#"))
      header.append($("<th></th>").text("Player"))
      header.append($("<th></th>").attr("title", "Win loss ratio").text("PPG"))
      header.append($("<th></th>").attr("title", "Games played").text("GP"))
      header.append($("<th></th>").attr("title", "Wins").text("W"))
      header.append($("<th></th>").attr("title", "Losses").text("L"))
      header.append($("<th></th>").attr("title", "Overtime wins").text("OTL"))
      header.append($("<th></th>").attr("title", "Goals for").text("GF"))
      header.append($("<th></th>").attr("title", "Goals against").text("GA"))
      header.append($("<th></th>").attr("title", "Goal difference").text("DIFF"))
      $("#overall_ppg").html("")
      $("#overall_ppg").append($("<caption></caption>").text("OVERALL PPG"))
      $("#overall_ppg").append(header)
      i = 0
      data["stats"].forEach(function(stats, i) {
        var row = $("<tr></tr>").addClass("player")
        row.append($("<th></th>").text(i+1))
        row.append($("<th></th>").text(stats["Player"]))
        row.append($("<th></th>").text(stats["PPG"]))
        row.append($("<th></th>").text(stats["GP"]))
        row.append($("<th></th>").text(stats["W"]))
        row.append($("<th></th>").text(stats["L"]))
        row.append($("<th></th>").text(stats["OTL"]))
        row.append($("<th></th>").text(stats["GF"]))
        row.append($("<th></th>").text(stats["GA"]))
        row.append($("<th></th>").text(stats["DIFF"]))
        $("#overall_ppg").append(row)
      })
    },
    complete: function() {
      setTimeout(loadOverallPpg, 3000)
    }
  });
}

function loadGamesPlayed() {
  $.ajax({
    url: $SCRIPT_ROOT + "/game_results",
    success: function(data) {
      var header = $("<tr></tr>").addClass("header")
      header.append($("<th></th>").text("Player"))
      header.append($("<th></th>").text("Player"))
      header.append($("<th></th>").text("Games played"))
      header.append($("<th></th>").text("Full time results"))
      header.append($("<th></th>").text("Overtime results"))
      $("#game_results").html("")
      $("#game_results").append($("<caption></caption>").text("GAMES PLAYED " + data["season"]))
      $("#game_results").append(header)
      data["results"].forEach(function(interResult) {
        var row = $("<tr></tr>").addClass("player")
        row.append($("<th></th>").text(interResult["playerA"]))
        row.append($("<th></th>").text(interResult["playerB"]))
        row.append($("<th></th>").text(interResult["games"]))
        row.append($("<th></th>").text(interResult["playerA_full_wins"] + " - " + interResult["playerB_full_wins"]))
        row.append($("<th></th>").text(interResult["playerA_overtime_wins"] + " - " + interResult["playerB_overtime_wins"]))
        $("#game_results").append(row)
      })
    },
    complete: function() {
      setTimeout(loadGamesPlayed, 3000)
    }
  });
}

function loadRecentGames() {
  $.ajax({
    url: $SCRIPT_ROOT + "/recent_games",
    success: function(data) {
      var header = $("<tr></tr>").addClass("header")
      header.append($("<th></th>").text("Home"))
      header.append($("<th></th>").text("Away"))
      header.append($("<th></th>").text("Result"))
      header.append($("<th></th>").text("Date"))
      $("#recent_games").html("")
      $("#recent_games").append($("<caption></caption>").text("RECENTLY PLAYED GAMES"))
      $("#recent_games").append(header)
      data.forEach(function(game) {
        var row = $("<tr></tr>").addClass("player")
        row.append($("<th></th>").text(game["home"]))
        row.append($("<th></th>").text(game["away"]))
        row.append($("<th></th>").text(game["home_score"] + " - " + game["away_score"] + (game["overtime"] == '\1' ? " (OT)" : "")))
        row.append($("<th></th>").text(game["date"]))
        $("#recent_games").append(row)
      })
    },
    complete: function() {
      setTimeout(loadRecentGames, 3000)
    }
  });
}

function loadPpgChampions() {
  $.ajax({
    url: $SCRIPT_ROOT + "/ppg_champions",
    success: function(data) {
      var header = $("<tr></tr>").addClass("header")
      header.append($("<th></th>").text("Season"))
      header.append($("<th></th>").text("Player"))
      header.append($("<th></th>").attr("title", "Points per game").text("PPG"))
      header.append($("<th></th>").attr("title", "Games played").text("GP"))
      header.append($("<th></th>").attr("title", "Wins").text("W"))
      header.append($("<th></th>").attr("title", "Losses").text("L"))
      header.append($("<th></th>").attr("title", "Overtime wins").text("OTW"))
      header.append($("<th></th>").attr("title", "Overtime losses").text("OTL"))
      header.append($("<th></th>").attr("title", "Goals for").text("GF"))
      header.append($("<th></th>").attr("title", "Goals against").text("GA"))
      header.append($("<th></th>").attr("title", "Goal difference").text("DIFF"))
      $("#ppg_champions").html("")
      $("#ppg_champions").append($("<caption></caption>").text("PPG CHAMPIONS"))
      $("#ppg_champions").append(header)

      var recordSeason;
      $.ajax({
        url: $SCRIPT_ROOT + "/ppg_record",
        async: false,
        success: function(data) {
          recordSeason = data["season"]
        }
      });

      console.log("recordSeason after ajax: " + recordSeason)
      data.forEach(function(champion) {
        var row = $("<tr></tr>").addClass("player")
	if (champion["season"] === recordSeason) {
	  row.addClass("record")
	}
        row.append($("<th></th>").text(champion["season"].replace(/_/g, ' ')))
        row.append($("<th></th>").text(champion["stats"]["Player"]))
        row.append($("<th></th>").text(champion["stats"]["PPG"]))
        row.append($("<th></th>").text(champion["stats"]["GP"]))
        row.append($("<th></th>").text(champion["stats"]["W"]))
        row.append($("<th></th>").text(champion["stats"]["L"]))
        row.append($("<th></th>").text(champion["stats"]["OTW"]))
        row.append($("<th></th>").text(champion["stats"]["OTL"]))
        row.append($("<th></th>").text(champion["stats"]["GF"]))
        row.append($("<th></th>").text(champion["stats"]["GA"]))
        row.append($("<th></th>").text(champion["stats"]["DIFF"]))
        $("#ppg_champions").append(row)
      })
    },
    complete: function() {
      setTimeout(loadPpgChampions, 3000)
    }
  });
}

$(function() {
  $.ajaxSetup({ cache: false })
  loadLeagueTitle()
  loadLeagueTitle()
  loadPpgStats()
  loadOverallPpg()
  loadGamesPlayed()
  loadRecentGames()
  loadPpgChampions()
})
