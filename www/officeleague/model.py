from officeleague import app

from flaskext.mysql import MySQL
from flask import jsonify, json
import pymysql
import decimal
import calendar
import datetime
import operator

import os

class DecimalJSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            # Convert decimal instances to strings.
            return str(obj)
        return super(DecimalJSONEncoder, self).default(obj)

app.json_encoder = DecimalJSONEncoder

mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = os.environ.get('MYSQL_DATABASE_USER')
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get('MYSQL_DATABASE_PASSWORD')
app.config['MYSQL_DATABASE_DB'] = os.environ.get('MYSQL_DATABASE_DB')
app.config['MYSQL_DATABASE_HOST'] = os.environ.get('MYSQL_DATABASE_HOST')
mysql.init_app(app)

def _execute_queries(queries, fetchone=False, commit=False):
    if not isinstance(queries, list):
        queries = [queries]
    conn = mysql.get_db()
    if not conn:
        conn = mysql.connect()
    cursor = conn.cursor(pymysql.cursors.DictCursor)
    for query in queries:
        cursor.execute(query)
    rv = cursor.fetchone() if fetchone else cursor.fetchall()
    if commit:
        conn.commit()
    cursor.close()
    return rv

def get_league_title():
    return jsonify(os.environ.get('MYSQL_DATABASE_DB').replace('_', ' ').upper())

def get_wlr_stats(year, month):
    return jsonify(_get_wlr_stats(year, month))

def get_overall_ppg_stats():
    stats = _get_overall_ppg_stats()["stats"]
    stats = [ stat for stat in stats if stat["GP"] > 50 ]
    return jsonify({ "stats" : stats })

def get_ppg_stats(year, month):
    return jsonify(_get_ppg_stats(year, month))

def get_ppg_champions(excluded_seasons):
    return jsonify(_get_ppg_champions(excluded_seasons))

def _get_ppg_champions(excluded_seasons):
    champions = []
    seasons = [ s for s in _get_seasons() if season_key(s["year"], s["month"]) not in excluded_seasons ]
    for season in seasons:
       stats = _get_ppg_stats(season["year"], season["month"])["stats"]
       stats = [ stat for stat in stats if stat["GP"] > 8 ]
       champions.append({ "season" : season_title(season["year"], season["month"])
                        , "stats" : stats[0]
                        })
    return list(reversed(champions))

def get_ppg_record():
    ppg_champions = _get_ppg_champions(exclude_current_season())
    ppg_champions.sort(key = lambda champion: (champion["stats"]["PPG"], champion["stats"]["DIFF"], champion["stats"]["GP"]), reverse=True)
    return jsonify(ppg_champions[0])

def get_game_results(year, month):
    return jsonify(_get_results(year, month))

def get_recent_games():
    return jsonify(_get_recent_games())

def season_key(year, month):
    return "{0}_{1}".format(year, month)

def season_title(year, month):
    return "{0} {1}".format(year, calendar.month_abbr[month].upper())

def get_players():
    return [ player["pname"] for player in _get_players() ]

def get_played_game_editions():
    return [ game["gname"] for game in _get_played_game_editions() ]

def get_current_game_edition():
    return _get_current_game_edition()["gname"]

def add_game(home, away, home_score, away_score, gname, overtime):
    queries = [ '''INSERT IGNORE INTO players (pname)
                   VALUES ('{0}')
                '''.format(home.upper())
              , '''INSERT IGNORE INTO players (pname)
                   VALUES ('{0}')
                '''.format(away.upper())
              ]
    _execute_queries(queries, commit=True)
    home_id = _get_player_id(home)
    away_id = _get_player_id(away)
    _execute_queries('''INSERT INTO games (home, away, home_score, away_score, gname, overtime)
                        VALUES ({0}, {1}, {2}, {3}, '{4}', {5})
                     '''.format(home_id, away_id, home_score, away_score, gname, 1 if overtime else 0), commit=True)

def create_table():
    queries = [ '''SET sql_notes = 0'''
              , '''CREATE TABLE IF NOT EXISTS games (
                       id MEDIUMINT NOT NULL AUTO_INCREMENT,
                       home MEDIUMINT NOT NULL,
                       away MEDIUMINT NOT NULL,
                       home_score TINYINT UNSIGNED NOT NULL,
                       away_score TINYINT UNSIGNED NOT NULL,
                       overtime BIT NOT NULL,
                       timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                       gname CHAR(15) NOT NULL,
                       PRIMARY KEY (id)
                   )
                '''
              , '''CREATE TABLE IF NOT EXISTS players (
                       id MEDIUMINT NOT NULL AUTO_INCREMENT,
                       pname CHAR(15) NOT NULL,
                       PRIMARY KEY (id),
                       UNIQUE (pname)
                   )
                '''
              , '''SET sql_notes = 1'''
              ]
    _execute_queries(queries)

def current_season():
    now = datetime.datetime.now()
    return (now.year, now.month)

def exclude_current_season():
    (year, month) = current_season()
    return [season_key(year, month)]

def _get_players():
    return _execute_queries('''SELECT
                                   id, pname
                               FROM
                                   players
                            ''')

def _get_played_game_editions():
    return _execute_queries('''SELECT DISTINCT
                                   gname
                               FROM
                                   games
                            ''')

def _get_current_game_edition():
    return _execute_queries('''SELECT
                                   gname, timestamp
                               FROM
                                   games
                               WHERE
                                   timestamp = (SELECT MAX(timestamp) FROM games)
                            ''')[0]

def _get_player_id(player_name):
    players = _get_players()
    for player in players:
        if player["pname"] == player_name:
            return player["id"]

def _get_ppg_stats(year, month):
    stats = _execute_queries('''SELECT
                                    pname AS Player,
                                    SUM(GP) AS GP,
                                    TRUNCATE((SUM(W) * 3 + SUM(OTW) * 2 + SUM(OTL)) / SUM(GP), 2) AS PPG,
                                    SUM(W) AS W,
                                    SUM(L) AS L,
                                    SUM(OTW) AS OTW,
                                    SUM(OTL) AS OTL,
                                    SUM(GF) as GF,
                                    SUM(GA) AS GA,
                                    SUM(DIFF) AS DIFF
                                FROM(
                                    SELECT
                                        home player,
                                        1 GP,
                                        IF(home_score > away_score AND overtime = (0),1,0) W,
                                        IF(home_score < away_score AND overtime = (0),1,0) L,
                                        IF(home_score > away_score AND overtime = (1),1,0) OTW,
                                        IF(home_score < away_score AND overtime = (1),1,0) OTL,
                                        home_score GF,
                                        away_score GA,
                                        CAST(home_score AS SIGNED)-CAST(away_score AS SIGNED) DIFF
                                    FROM games
                                    WHERE YEAR(timestamp) = {0} AND MONTH(timestamp) = {1}
                                    UNION ALL
                                    SELECT
                                        away,
                                        1,
                                        IF(home_score < away_score AND overtime = (0),1,0),
                                        IF(home_score > away_score AND overtime = (0),1,0),
                                        IF(home_score < away_score AND overtime = (1),1,0),
                                        IF(home_score > away_score AND overtime = (1),1,0),
                                        away_score,
                                        home_score,
                                        CAST(away_score AS SIGNED)-CAST(home_score AS SIGNED) DIFF
                                    FROM games
                                    WHERE YEAR(timestamp) = {0} AND MONTH(timestamp) = {1}
                                ) as tot
                                JOIN players p ON tot.Player=p.id
                                GROUP BY Player
                                ORDER BY PPG DESC, DIFF DESC, GP DESC'''.format(year, month))
    return { "season" : "{0} {1}".format(year, calendar.month_abbr[month].upper())
           , "stats"  : stats
           }

def _get_overall_ppg_stats():
    stats = _execute_queries('''SELECT
                                    pname AS Player,
                                    SUM(GP) AS GP,
                                    TRUNCATE((SUM(W) * 2 + SUM(OTL)) / SUM(GP), 2) AS PPG,
                                    SUM(W) AS W,
                                    SUM(L) AS L,
                                    SUM(OTL) AS OTL,
                                    SUM(GF) as GF,
                                    SUM(GA) AS GA,
                                    SUM(DIFF) AS DIFF
                                FROM(
                                    SELECT
                                        home player,
                                        1 GP,
                                        IF(home_score > away_score,1,0) W,
                                        IF(home_score < away_score AND overtime = (0),1,0) L,
                                        IF(home_score < away_score AND overtime = (1),1,0) OTL,
                                        home_score GF,
                                        away_score GA,
                                        CAST(home_score AS SIGNED)-CAST(away_score AS SIGNED) DIFF
                                    FROM games
                                    UNION ALL
                                    SELECT
                                        away,
                                        1,
                                        IF(home_score < away_score,1,0),
                                        IF(home_score > away_score AND overtime = (0),1,0),
                                        IF(home_score > away_score AND overtime = (1),1,0),
                                        away_score,
                                        home_score,
                                        CAST(away_score AS SIGNED)-CAST(home_score AS SIGNED) DIFF
                                    FROM games
                                ) as tot
                                JOIN players p ON tot.Player=p.id
                                GROUP BY Player
                                ORDER BY PPG DESC, DIFF DESC, GP DESC''')
    return { "stats"  : stats }

def _get_seasons():
    return _execute_queries('''SELECT
                                   DISTINCT YEAR(timestamp) AS year, MONTH(timestamp) AS month
                               FROM
                                   games
                               ORDER BY
                                   YEAR(timestamp), MONTH(timestamp)
                            ''')

def _get_results(year, month):
    results = _execute_queries('''SELECT
                                      playerA,
                                      playerB,
                                      COUNT(CASE WHEN (pidA = home AND pidB = away) OR
                                           (pidA = away AND pidB = home) THEN 1 END) as games,
                                      COUNT(CASE WHEN ((pidA = home AND pidB = away AND home_score > away_score) OR
                                                       (pidA = away AND pidB = home AND home_score < away_score)) AND
                                                       overtime != (1) THEN 1 END) as playerA_full_wins,
                                      COUNT(CASE WHEN ((pidA = home AND pidB = away AND home_score < away_score) OR
                                                       (pidA = away AND pidB = home AND home_score > away_score)) AND
                                                        overtime != (1) THEN 1 END) as playerB_full_wins,
                                      COUNT(CASE WHEN ((pidA = home AND pidB = away AND home_score > away_score) OR
                                                       (pidA = away AND pidB = home AND home_score < away_score)) AND
                                                       overtime = (1) THEN 1 END) as playerA_overtime_wins,
                                      COUNT(CASE WHEN ((pidA = home AND pidB = away AND home_score < away_score) OR
                                                       (pidA = away AND pidB = home AND home_score > away_score)) AND
                                                        overtime = (1) THEN 1 END) as playerB_overtime_wins
                                  FROM
                                      (SELECT
                                          a.id AS pidA,
                                          b.id AS pidB,
                                          a.pname AS playerA,
                                          b.pname AS playerB
                                      FROM
                                          players a
                                          CROSS JOIN players b
                                      WHERE
                                          a.id < b.id) as per
                                      JOIN games
                                  WHERE YEAR(timestamp) = {0} AND MONTH(timestamp) = {1}
                                  GROUP BY
                                      playerA, playerB
                                  HAVING
                                      games > 0
                                  ORDER BY
                                      games DESC, playerA, playerB
                               '''.format(year, month))
    return { "season" : "{0} {1}".format(year, calendar.month_abbr[month].upper())
           , "results"  : results
           }

def _get_recent_games():
    return _execute_queries('''SELECT
                                   p1.pname as home,
                                   p2.pname as away,
                                   home_score,
                                   away_score,
                                   overtime,
                                   DATE_FORMAT(timestamp,'%Y-%m-%d') as date
                               FROM
                                   games a
                                   JOIN (SELECT
                                             if(home > away, home, away) playerA,
                                             if(home > away, away, home) playerB,
                                             MAX(id) id
                                         FROM games
                                         GROUP BY
                                             playerA, playerB) b ON a.id = b.id
                                   JOIN players p1 ON home = p1.id
                                   JOIN players p2 ON away = p2.id
                               ORDER BY
                                   timestamp DESC''')
